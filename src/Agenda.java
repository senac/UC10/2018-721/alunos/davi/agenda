
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class Agenda {

    public static void main(String[] args) {

        // Davi Covre 
        
        List<Contato> contatos = new ArrayList<>();
        String aux;
        int opcao;
        Contato contato = null;
        String nome;
        String telefone;
        for (;;) {
            boolean achei = false;

            String menu = String.format("########### Agenda de Contatos ###########\n"
                    + "                                Contatos(%d)\n"
                    + "1 - Novo Contato\n"
                    + "2 - Buscar Contato por Nome\n"
                    + "3 - Remover Contato\n"
                    + "4 - Lista Todos Contatos\n"
                    + "5 - Sobre Agenda de Contatos\n"
                    + "0 - Sair", contatos.size());

            aux = JOptionPane.showInputDialog(menu);
            opcao = Integer.parseInt(aux);

            switch (opcao) {
                case 1:
                    nome = JOptionPane.showInputDialog("Nome");

                    for (int i = 0; i < contatos.size(); i++) {
                        if (nome.equalsIgnoreCase(contatos.get(i).nome)) {
                            achei = true;
                        }
                    }

                    if (!achei) {
                        telefone = JOptionPane.showInputDialog("Telefone");
                        contato = new Contato();
                        contato.nome = nome;
                        contato.telefone = telefone;

                        contatos.add(contato);

                        JOptionPane.showMessageDialog(null, "Salvo com sucesso!");
                    } else {
                        JOptionPane.showMessageDialog(null, "Contato ja cadastrado!!!");
                    }

                    break;

                case 2:
                    if (!contatos.isEmpty()) {
                        nome = JOptionPane.showInputDialog("Digite Nome a ser buscado");
                        for (int i = 0; i < contatos.size(); i++) {
                            contato = contatos.get(i);
                            if (contato.nome.equals(nome)) {
                                achei = true;
                            }
                        }

                        if (achei) {
                            JOptionPane.showMessageDialog(null, contato.nome + "\n" + contato.telefone);
                        } else {
                            JOptionPane.showMessageDialog(null, "Nome pesquisado nao encontrado!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Lista Vazia!");
                    }

                    break;

                case 3:
                    if (!contatos.isEmpty()) {
                        int posicao = 0;
                        nome = JOptionPane.showInputDialog("Digite nome a ser removido");
                        for (int i = 0; i < contatos.size(); i++) {
                            contato = contatos.get(i);
                            if (contato.nome.equals(nome)) {
                                achei = true;
                                posicao = i;
                            }
                        }

                        if (achei) {

                            contatos.remove(posicao);
                            JOptionPane.showMessageDialog(null, "Removido com sucesso!");

                        } else {
                            JOptionPane.showMessageDialog(null, "Nome pesquisado nao encontrado!");
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Lista Vazia!");
                    }

                    break;

                case 4:
                    //“NomeContato – (22)3222-2222” 
                    String listaDeContatos = new String();

                    for (int i = 0; i < contatos.size(); i++) {
                        listaDeContatos += contatos.get(i).nome + " – " + contatos.get(i).telefone + "\n";
                    }

                    JOptionPane.showMessageDialog(null, listaDeContatos);

                    break;

                case 5:

                    String sobre = "Desenvolvida por : Jose da Silva\n"
                            + "Turma: 721 de 2018 ";

                    JOptionPane.showMessageDialog(null, sobre);

                    break;

                case 0:
                    System.exit(0);

            }

        }

    }

}
